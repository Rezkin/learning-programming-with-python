# Learning Programming with Python

[[_TOC_]]

---

## Welcome

**This guide is still a work-in-progress, please bear with me as I refine it and add more content.**

Hello and welcome to this guide on learning to program with Python!  The intended audience of this project is for someone with an interest but very little to no knowledge on the practical aspects of computer programming.  If you are already familiar with other programming languages, this will probably not be the guide for you since it will go in much greater detail on concepts rather than just syntax.  If not, and you are still interested, pull up a chair (so to speak) and we'll get started.

Now, you already found this project and are reading this document so that's a good start!  This document will explain everything about this project and act as a guide for you to make use of the examples, install of the necessary software, and generally keep you on track as you learn how computer programming works.

---

## Goal of this Project

The ultimate goal of this project is to teach you about programming and Python and by the end have a working program which will use "Optical Text Recognition" to read typed and handwritten text in images and transform it into a text file.  In other words, you can take a picture of a document and extract the text into a file.  Pretty cool, right?

---

## How this Project Works

### Lessons

Each lesson is broken into sections using what are known as `branches`.  This project uses a program called `git` to manage different versions of the code.  This is not required, but for more information on what `git` is check out the [Prequisite Software - Git](#git---optional) section below.

Within each branch there will be a `README.md` file (just like this) to explain the concepts and help you understand what to do for the lesson.  If you are exploring via the web browser, you can change branches to go to different lessons like this:

![GitLab Change Branches](./GitLabChangeBranch.PNG)

Each lesson will contain it's own glossary in the `README` with any terms and concepts introduced in the lesson, but the main `README` here will always have all of the terms throughout every lesson broken down so you can easily search them.

### Lesson Requirements

As each lesson builds on itself, you will need to occasionally install different packages or programs to use code from those programs to run the application.  Always make sure to check the `README.md` for instructions on how to download these if there are any.  This is done in pieces so the concepts can be explains rather than making you download a bunch of propgrams right at the start.

### Important Files

Aside from the `README` file in each lesson, there is a `lesson.py` file containing all of the code within the lesson, and a [main.py](./main.py) file containing the building blocks of our final program.  The branch you are in now is called the `main` branch and contains all of the final, completed code for this project if you wanted to see what the final version looks like.

---

## Prerequisite Software

### Python - Required

Since this project uses Python, you will need to install Python onto your machine.  You can download it from the official Python website by clicking [here](https://www.python.org/downloads/).

Installing it with the default options is fine.

### IDE - Optional

Next, you will need to install your IDE (Integrated Development Environment).  These are more of a personal preference choice, as you could very well use a text editor or Notepad to edit your code.  This is basically a tool to let you write your code more easily and see all of the code in your project.  Some IDEs let you step into your code while it's running to help you fix problems.

Recommendations:

1. [Visual Studio Code](https://code.visualstudio.com/Download) - An all-around IDE that supports any file type.  It's small, easy to set up, and very customizable if you are into that sort of thing.  If you do install this, you will want to also install the Python extension.  You can do this by going to the "Extensions" view within Visual Studio Code, or clicking "Install" from the extension on the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ms-python.python).
1. [PyCharm](https://www.jetbrains.com/pycharm/) - Pycharm is a bit of a larger download, but it's customized and designed to work with Python.  It can be overwhelmning at first, but it's also very powerful and has a lot of useful features for debugging your code.
1. [Notepad++](https://notepad-plus-plus.org/downloads/) - If the idea of an IDE is too much for you, you can also just use something simple like Notepad++.  It's a simple editor with few distractions and lets you focus on one file at a time.  However, you will need to run Python yourself via the command line since Notepad++ is _just_ a fancy text editor, not a full IDE.

### Git - Optional

Git is the program this project / repository uses to manage the different versions via `branches`.  This lets you install the code locally on your machine and switch between different lessons easily.  If you intend to just explore the repository on the web, you won't need to install this.  You can view all of the tutorials and code in your web-browser.

1. For Linux - This should already be installed.  If not, you will need to use your package manager of choice to install `git`.  For instance, `apt-get install git` or `apk add git`.  Instructions [here](https://git-scm.com/download/linux).
1. For MacOS - If you have Homebrew installed, you can simply do `brew install git`.  You can find more instructions [here](https://git-scm.com/download/mac).
1. For Windows, you will want to install [Git for Windows](https://gitforwindows.org/).  Windows doesn't have an easily accessible command line interface, so installing the Git Bash GUI lets you run commands like Linux and MacOS users would without having to use the rather annoying CMD utility the regular Git installation uses.

Git can be rather complicated for first timers, but I highly recommend learning if you are serious about wanting to learn to program.  Nearly all proffesional developers use it on a daily basis.  There is a super fun and interactive website that walks you through how git works if you are interested in learning.  Check it out at [https://learngitbranching.js.org/](https://learngitbranching.js.org/).

---

## Glossary

Here you will find any terms used throughout these lessons in one easy-to-find location.  They are organized by lesson to help you know where to go if you wanted to learn more about those concepts.

### General

### Programming Language

A programming language is simply a way for a human to write a set of instructions for a computer to execute.  At it's core, a programming languge will often get turned into another language called "Assembly" which is the most basic kind of computer instruction.  A programming language like Python is simply a translator of sorts to make it easy for humans to input instructions to the computer.  There are hundreds of different kinds of programming languages, each with their own strengths and weaknesses.  However, the core concepts you learn apply to pretty much every language out there.  Once you learn one programming language, it's much easier to learn the next one.  Similiar to learning to speak a new language, where you can transfer the knowledge of things like "nouns" and "verbs", you can transfer the knowledge of "while loops" and "functions" from one programming language to another.

### Packages

A package is a set of code that you can add to your software to allow you to do some action.  For instance, Python has a package called `requests` for allowing you to call web URLs to get information from the internet within your Python program.  While you could do this yourself, it's much easier to just install a package containing all of that code for you.  Think of it like buying firewood.  You _could_ chop wood yourself, but buying firewood lets you skip the hard work and just use the finished product.  Most programming languages have some kind of package manager to let you easily install these packages into your software.  Python has `pip`, C# uses `NuGet`, Java uses `gradle` or `Maven`, Ruby uses `gems` and so on.

Unless there are special circumstances, most packages across most programming languages you download are free to use and do not require any sort of payment.  Everything we use in this project is free.

### Lesson 1 - Variables and Operators

1. `variable =`
    * The equals sign allows you to assign a value to a variable.  The left-hand side is your variable name, followed by the equals, followed by the value you want the variable to have.
    * Example:  `x = 12`
    * Example:  `full_name = "Larry Sandiego"`
1. `print("some value")`
    * Allows you to output whatever is within the `""`` to the screen.
    * Example:  `print("Hello World")`
    * Example:  `print("My First Program")`
1. `print(myVariableName)`
    * Allows you to print a variable to the screen.
    * Example:  `print(example)` would print `"John Doe"` if `example = "John Doe"`.
1. `integer`
    * An integer is a date type representing all positive and negative whole numbers.  -100, 0, 17, and 100,008 are all integers, but 1.5 is _not_.
1. `double` / `float`
   * Doubles and floats refer to the same type, but just different sizes.  A float is smaller than a double in terms of how much memory it takes up.  They are sometimes used interchangebly when referring to the data type itself.
   * Doubles are all positive and negative floating point numbers.  So any number with a decimal part after it, like 0.25, -23.7, 8,420.987654321.
   * Whole numbers can also be doubles, but they will be stored with a decimal part.  For instance, 10 would be stored as 10.0.
1. `boolean`
    * A `bool` or `boolean` is a `True` or `False` value (note the capital `T` and `F`).
    * Example:  `my_variable = True`
    * Fun fact:  They are called `boolean` after [George Bool](https://en.wikipedia.org/wiki/George_Boole) who first wrote a book on mathematics and logic in 1847 where he introduced the concept of true and false values in mathematics.  We still call them `bools` to this day.
1. `string`
    * A string is any arbitrary series of characters.  It can be of any length, including empty.  They are typically denoted between two double quotes `""`.
    * Example:  `""` -> An empty string that contains no characters.
    * Example: `"This is an example of a string!"`
1. `+`
    * Adds two numbers.
    * Example:  `1 + 1 = 2`
1. `-`
    * Subtracts two numbers.
    * Example:  `2 - 2 = 0`
1. `/`
    * Divides two numbers.
    * Example:  `10 / 5 = 2`
1. `*`
    * Multiplies two numbers.
    * Example:  `3 * 3 = 9`
1. `**`
    * Used for performing exponents such as 2 to the power of 3.  You may have also seen it as 2^3 in calculators.
    * Example:  `2**4 = 16`
    * Example: `3**2 = 9`
1. `%`
    * The "modulus" operator will return the remainder of division.  4 % 2 returns 0 because there is no remainder, but 5 % 3 returns 2 since there are 2 left over if we try to divide 5 by 3.
    * Example:  `10 % 2 = 0`
    * Example:  `9 % 3 = 0`
    * Example:  `9 % 2 = 1`
    * Example:  `7 % 5 = 2`

### Lesson 2 - Conditionals

TODO

---

## About the Author

Hello!  My name is Jackson Murrell and I've been doing software development professionally for the better part of five years now.  I've always had a passion for computer science and programming and now I get to share that with you, the reader!  This all started from my desire to teach a few classes at my local library, and I wanted to make a guide to go along with, so this project was born.

### Contact

If you have any questions and would like to reach out to me directly, feel free to at `jackson.m.murrell@protonmail.com`.  I'd be happy to answer any questions you may have.
