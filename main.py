# Python built-ins.
import argparse
import pdb
import sys
import os
from typing import Any

# Third-party libraries.
from PIL import Image
import pytesseract
import cv2
import numpy as np

# Status codes to return.
# These won't be seen by the user, but these can be intepreted by other programs to know if
# something went wrong.
# 0 is the standard success code.
SUCCESS = 0
# Any error codes are normally determined by the program (us).
FILE_ERROR = 1


def post_mortem_exception_handler(exception_type, exception,
                                  traceback, except_hook=sys.excepthook) -> None:
    """Starts a post-mortem debug shell at the point of failure.

    This processes exceptions normally in all other ways.

    Args:
        exception_type (Type[BaseException]): Exception type being raised.
        exception (BaseException): Exception instance being raised.
        traceback (TracebackType): Traceback of the exception which is used to open a debug shell.

    KArgs:
        except_hook (Function, optional): Exception handler to add debugging to.
        Defaults to sys.excepthook.
    """
    except_hook(exception_type, exception, traceback)
    pdb.post_mortem(traceback)


def read_text(image_path: str):
    """Processes an image into text using optical text recognition.

    Args:
        input_file (str): The file path to the image to process.
    """
    # Load the image using opencv.
    img = cv2.imread(image_path)
    # Resize the image.  The Tesseract documentation recommends at least a 300 DPI image for best
    # results.
    img = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)

    # Configure the tesseract process to provide better results.
    config = '--oem 3 --psm 11'
    text = pytesseract.image_to_string(img, config = config, lang='eng')
    return text


def main() -> int:
    """The entry point into the application which handles argument parsing and running the
    appropriate logic.

    Returns:
        int: The status code of the application to be returned to the system.
    """
    parser = argparse.ArgumentParser(description="Given an image, extracts any written text and" +
                                     "outputs it into a file using optical character recognition.")
    parser.add_argument("-d", "--debug", required=False, action="store_true",
                        help="Enables a PDB post-mortem traceback for diagnosing and debugging " +
                        "errors.")
    parser.add_argument("-i", "--input", required=True, type=str,
                        help="The input file to extract text from.")
    parser.add_argument("-o", "--output", required=True, type=str,
                        help="The output file to save text into.")

    # Parse the arguments into argparse's namespace, and then turn them into a dictionary.
    # This way, we can pop arguments off and pass around the whole set to classes via *args or
    # **kargs if need be.
    args_dict = vars(parser.parse_args())

    # If desired, set the system's exception handler to the post-mortem handler for easier
    # debugging.
    if args_dict.get("debug"):
        sys.excepthook = post_mortem_exception_handler

    # Get our input and output files and get rid of any extra whitespace at the beginning or end.
    input_file_path = args_dict["input"].strip()
    output_file_path = args_dict["output"].strip()

    # Check to see if the user-provided input path exists on the computer.
    # If it doesn't, print an error and exit.
    if not os.path.exists(input_file_path):
        print("Provided path \"" + input_file_path + "\" doesn't exist.")
        return FILE_ERROR

    # Do the same hing in reverse.  For the output path, we don't want a file to already exist.
    # If it does, throw an error.
    if os.path.exists(output_file_path):
        print("Provided path \"" + output_file_path + "\" already exists.  Please remove or " +
              "rename the file.")
        return FILE_ERROR

    # Call our method to read in the text.
    processed_text = read_text(input_file_path)
    # Output the found text to the console.
    print("===Text Found:===")
    print(processed_text)

    # And finally, save the results to the file.
    # If the file doesn't exist, it will be created for us.
    with open(output_file_path, 'w') as output_file:
        output_file.write(processed_text)
        print("===Saved file at " + output_file_path + "===")

    # Return a success status code since nothing went wrong.
    return SUCCESS


# Only run main if we are executing this file directly.  Without putting it in a function, any code
# inside this file would also be run if it is imported.  This if statement checks for that.
if __name__ == "__main__":
    sys.exit(main())
